from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    price = models.IntegerField(blank=False, null=False)
    category = models.ForeignKey('category.Category', on_delete=models.CASCADE)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return f'Name of product is: {self.name}'
