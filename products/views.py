from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from products.models import Product


class ProductCreateView(LoginRequiredMixin, CreateView):
    template_name = 'product/product_create.html'
    model = Product
    fields = '__all__'
    success_url = reverse_lazy('product:product_list')
    # form_class =


class ProductListView(LoginRequiredMixin, ListView):
    template_name = 'product/product_list.html'
    model = Product
    context_object_name = 'all_products'


class ProductUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'product/product_update.html'
    model = Product
    fields = '__all__'
    # form_class = ProductForm
    success_url = reverse_lazy('product:product_list')


class ProductDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'product/product_delete.html'
    model = Product
    success_url = reverse_lazy('product:product_list')