from django.urls import path, include

from permissions.views import PermissionCreateView

app_name = 'permissions'

urlpatterns = [
    path('permission-create/', PermissionCreateView.as_view(), name='permission_create')
]
