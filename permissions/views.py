from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Permission
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView


class PermissionCreateView(LoginRequiredMixin, CreateView):
    template_name = 'permissions/permissions_create.html'
    model = Permission
    fields = '__all__'
    success_url = reverse_lazy('login')