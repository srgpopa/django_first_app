from django.urls import path, include

from home.views import HomeTemplateView

urlpatterns = [
    path('', HomeTemplateView.as_view(), name='home_page')
]
