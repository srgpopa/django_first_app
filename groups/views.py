from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.contrib.auth.models import Group


class GroupCreateView(LoginRequiredMixin, CreateView):
    template_name = 'groups/groups_create.html'
    model = Group
    fields = '__all__'
    success_url = reverse_lazy('login')

@login_required #metoda se acceseaza logged in doar daca are decoratorul asta
def new_method():
    pass
