from django.urls import path, include

from groups.views import GroupCreateView

app_name = 'groups'

urlpatterns = [
    path('groups-create/', GroupCreateView.as_view(), name='groups_create')
]
