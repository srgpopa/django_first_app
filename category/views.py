from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from category.form import CategoryForm
from category.models import Category


class CategoryCreateView(LoginRequiredMixin, CreateView):
    template_name = 'category/category_create.html'
    model = Category
    # fields = '__all__'
    success_url = reverse_lazy('category:category_list')
    form_class = CategoryForm


class CategoryListView(LoginRequiredMixin, ListView):
    template_name = 'category/category_list.html'
    model = Category
    context_object_name = 'all_categories'


class CategoryUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'category/category_update.html'
    model = Category
    # fields = ['name', 'description'] #'__all__'
    form_class = CategoryForm
    success_url = reverse_lazy('category:category_list')


class CategoryDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'category/category_delete.html'
    model = Category
    success_url = reverse_lazy('category:category_list')
