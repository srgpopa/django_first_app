from django import forms
from django.forms import TextInput

from category.models import Category


class CategoryForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = '__all__'
        widgets = {
            'name':TextInput(attrs={
                'placeholder':'Insert a category name'
            }),
            'description':TextInput(attrs={
                'placeholder':'Insert description for category'
            })
        }

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = True
        self.fields['description'].required = True

    def clean(self):
        name = self.cleaned_data.get('name')
        description = self.cleaned_data.get('description')
        all_categories = Category.objects.all()
        for category in all_categories:
            if name == category.name:
                msg = 'Category already exists'
                self._errors['name'] = self.error_class([msg])