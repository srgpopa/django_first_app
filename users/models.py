from django.contrib.auth.models import User
from django.db import models


class ExtendUser(User):
    phone = models.CharField(max_length=255, blank=True, null=True)
    group = models.ForeignKey('auth.Group', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'