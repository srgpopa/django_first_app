from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from users.models import ExtendUser


class RegisterForm(UserCreationForm):
    class Meta:
        model = ExtendUser
        fields = ['first_name', 'last_name', 'email', 'username', 'group', 'phone']