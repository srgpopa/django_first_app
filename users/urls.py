from django.urls import path, include

from users.views import UserCreateView

urlpatterns = [
    path('user-create/', UserCreateView.as_view(), name='create_user')
]